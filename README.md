# Rendu "Un fichier de cartes bancaires"

## Binome

MEDAH, Lisa, email: lisa.medah.etu@univ-lille.fr


## Partie 1 : Responsabilité Partagée

- Pour chiffrer et dechiffrer le fichier contenant les noms et numéros de cartes des clients, on utilise une clé `cleprincipale` qui sera chiffrée deux fois; une fois avec `cle1` qui sera aussi chiffrée à l'aide d'un mot de passe ( cette cle appartient au responsable1 ) et qui sera stockée dans le dossier usb1. Une deuxieme fois avec `cle2` en suivant le meme principe qu'avec cle1 et cette cle `cle2` appartient au responsable2.
## Question 1

* 

## Question 2

* 

## Partie 2 : Délégation de droit


## Question 3

* 
 

## Question 4


##Partie 3 : Révocation de droit


## Question 5

* 

## Question 6

* 

