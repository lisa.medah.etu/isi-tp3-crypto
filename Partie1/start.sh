#! /bin/bash

# decrypter la clé du responsable1
echo "Responsable1 veuillez entrer votre mot de passe: "
openssl enc -d -pbkdf2 -aes-256-cbc -in ./usb1/cle1.enc -out ./ramdisk/cle1.clear

# decrypter la clé du responsable2
echo "Responsable2 veuillez entrer votre mot de passe: "
openssl enc -d -pbkdf2 -aes-256-cbc -in ./usb2/cle2.enc -out ./ramdisk/cle2.clear

# decrypter la clé principale et la mettre dans la ram
openssl enc -d -pbkdf2 -aes-256-cbc -in ./crypted/cleprincipale.enc -out ./ramdisk/cleprincipale1.enc -pass file:<(cat ./ramdisk/cle2.clear)


openssl enc -d -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale1.enc -out ./ramdisk/cleprincipale.clear -pass file:<(cat ./ramdisk/cle1.clear)


#suppression des clés en clair 
rm ./ramdisk/cle2.clear

rm ./ramdisk/cleprincipale1.enc

rm ./ramdisk/cle1.clear
