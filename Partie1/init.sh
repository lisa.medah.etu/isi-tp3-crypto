#! /bin/bash


#CReation des dossiers necessaires 
sudo mkdir ./ramdisk
mkdir usb1 usb2 crypted

#creation du dossier ramdisk dans la RAM
sudo mount -t tmpfs -o rw, size=1M tmpfs ./ramdisk

#creation du fichier cleprincipale.clear
dd if=/dev/random bs=32 count=1 > ./ramdisk/cleprincipale.clear

#creation du fichier cartes.clear contenant les noms des utilisateurs et les numeros de cartes 

touch ./ramdisk/cartes.clear

#cryptage du fichier cartes.clear
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cartes.clear -out ./crypted/cartes.enc -pass file:<(cat ./ramdisk/cleprincipale.clear)

#Suppression du fichier cartes.clear
rm ./ramdisk/cartes.clear



#Generation d'une cle aléatoire ( cle1.clear)
dd if=/dev/random bs=32 count=1 > ./ramdisk/cle1.clear



#Le responsable choisit un mot de passe pour protéger sa clé. La clé protégée est copiée sur sa clé usb 
openssl enc -pbkdf2 -aes-256-cbc -in ./ramdisk/cle1.clear -out usb1/cle1.enc

# cryptage de la cle principale par la cle1 du responsable1

openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale.clear -out ./ramdisk/cleprincipale1.enc -pass file:<(cat ./ramdisk/cle1.clear)



#suppression des deux clés en clair, cle1.clear et cleprincipale.clear
rm ./ramdisk/cle1.clear
rm ./ramdisk/cleprincipale.clear



#Geration de la deuxieme cle qui appartient au responsable2, cle2.clear
dd if=/dev/random bs=32 count=1 > ./ramdisk/cle2.clear


# Le responsable choisit un mot de passe pour protéger sa clé. La clé protégée est copiée sur sa clé usb 
openssl enc -pbkdf2 -aes-256-cbc -in ./ramdisk/cle2.clear -out usb2/cle2.enc


# crypter la cle principale par la cle2 du responsable2
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale1.enc -out ./crypted/cleprincipale.enc -pass file:<(cat ./ramdisk/cle2.clear)


#suppression des deux clés en clair
rm ./ramdisk/cle2.clear
rm ./ramdisk/cleprincipale1.clear

























