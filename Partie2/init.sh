#! /bin/bash


#CReation des dossiers necessaires 
sudo mkdir ./ramdisk
mkdir usb1 usb2 usb3 usb4crypted

#creation du dossier ramdisk dans la RAM
sudo mount -t tmpfs -o rw, size=1M tmpfs ./ramdisk

#creation du fichier cleprincipale.clear
dd if=/dev/random bs=32 count=1 > ./ramdisk/cleprincipale.clear

#creation du fichier cartes.clear contenant les noms des utilisateurs et les numeros de cartes 

touch ./ramdisk/cartes.clear

#cryptage du fichier cartes.clear
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cartes.clear -out ./crypted/cartes.enc -pass file:<(cat ./ramdisk/cleprincipale.clear)

#Suppression du fichier cartes.clear
rm ./ramdisk/cartes.clear

#Generation aléatoire des clés pour chacun des ayants droits
dd if=/dev/random bs=32 count=1 > ./ramdisk/cle1.clear
dd if=/dev/random bs=32 count=1 > ./ramdisk/cle2.clear
dd if=/dev/random bs=32 count=1 > ./ramdisk/cle3.clear
dd if=/dev/random bs=32 count=1 > ./ramdisk/cle4.clear


#Les responsables choisissent un mot de passe pour protéger leur clé. La clé protégée est copiée sur leur clé usb 
echo "Responsable Technique"
openssl enc -pbkdf2 -aes-256-cbc -in ./ramdisk/cle1.clear -out usb1/cle1.enc

echo "Responsable Juridique"
openssl enc -pbkdf2 -aes-256-cbc -in ./ramdisk/cle2.clear -out usb2/cle2.enc

echo "Representant du Responsable Technique"
openssl enc -pbkdf2 -aes-256-cbc -in ./ramdisk/cle3.clear -out usb3/cle3.enc

echo "Representant du Responsable Juridique"
openssl enc -pbkdf2 -aes-256-cbc -in ./ramdisk/cle4.clear -out usb4/cle4.enc


#Cryptage de  la cle princiaple par la cle du responsable technique : responsable1
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale.clear -out ./ramdisk/cleprincipale1.enc -pass file:<(cat ./ramdisk/cle1.clear)

# On supprime la clé du responsable technique en clair
rm ./ramdisk/cle1.clear


# Crypter une deuxième fois par la cle du responsable juridique : responsable2
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale1.enc -out ./crypted/cleprincipale12.enc -pass file:<(cat ./ramdisk/cle2.clear)

# Crypter une deuxième fois par la cle du  représentant du responsable juridique : responsable3 
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale1.enc -out ./crypted/cleprincipale14.enc -pass file:<(cat ./ramdisk/cle4.clear)

#suppression de la clé en clair 
rm ./ramdisk/cleprincipale1.enc

#Crypter la cle master par la cle du representant du responsable thechnique : responsable4
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale.clear -out ./ramdisk/cleprincipale3.enc -pass file:<(cat ./ramdisk/cle3.clear)

# On supprime la clé du respo3 en clair
rm ./ramdisk/cle3.clear

# Crypter une deuxième fois par la cle du responsable juridique
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale3.enc -out ./crypted/cleprincipale32.enc -pass file:<(cat ./ramdisk/cle2.clear)

# On supprime la clé du responsable2 en clair
rm ./ramdisk/cle2.clear

# Crypter une deuxième fois par la cle du representant du responsable juridique :  responsable 4
openssl enc  -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale3.enc -out ./crypted/cleprincipale34.enc -pass file:<(cat  ./ramdisk/cle4.clear)

# On supprime la clé du respo4 en clair
rm ./ramdisk/cle4.clear
rm ./ramdisk/cleprincipale3.enc
rm ./ramdisk/cleprincipale.clear






































