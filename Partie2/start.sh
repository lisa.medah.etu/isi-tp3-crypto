#!/bin/bash


# decrypter la clé du responsable1 ou son représentant
read -p "Veuillez entrer res1 si vous êtes le résponsabe technique, res2 si vous êtes son representant: " responsable1


if [ "$responsable1" == "res1" ]; then
	openssl enc -d -pbkdf2 -aes-256-cbc -in ./usb1/cle1.enc -out ./ramdisk/cle1.clear
elif [ "$responsable1" == "res2" ]; then
    openssl enc -d -pbkdf2 -aes-256-cbc -in ./usb3/cle3.enc -out ./ramdisk/cle3.clear
else 
    echo "Error"
    exit 
fi



read -p "Veuillez entrer res1 si vous êtes le résponsabe juridique, res2 si vous êtes son representant: " responsable2


if [ "$responsable2" == "res1" ]; then
	openssl enc -d -pbkdf2 -aes-256-cbc -in ./usb2/cle2.enc -out ./ramdisk/cle2.clear


elif [ "$responsable2" == "res2" ]; then
    	openssl enc -d -pbkdf2 -aes-256-cbc -in ./usb4/cle4.enc -out ./ramdisk/cle4.clear
else 
    echo "Error"
    exit 
fi




# Dechiffrer le bon fichier 


if [ "$responsable1" == "res1" ] && [ "$responsable2" == "res1" ]; then

	openssl enc -d -pbkdf2 -aes-256-cbc -in ./crypted/cleprincipale12.enc -out ./ramdisk/cleprincipale1.enc -pass file:<(cat ./ramdisk/cle2.clear)
    openssl enc -d -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale1.enc -out ./ramdisk/cleprincipale.clear -pass file:<(cat ./ramdisk/cle1.clear)
    rm ./ramdisk/cleprincipale1.enc
    rm ./ramdisk/cle1.clear
    rm ./ramdisk/cle2.clear



elif [ "$responsable1" == "res1" ] && [ "$responsable2" == "res2" ]; then

    openssl enc -d -pbkdf2 -aes-256-cbc -in ./crypted/cleprincipale14.enc -out ./ramdisk/cleprincipale1.enc -pass file:<(cat ./ramdisk/cle4.clear)
    openssl enc -d -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale1.enc -out ./ramdisk/cleprincipale.clear -pass file:<(cat ./ramdisk/cle1.clear)
    rm ./ramdisk/cleprincipale1.enc
    rm ./ramdisk/cle1.clear
    rm ./ramdisk/cle4.clear



elif [ "$responsable1" == "res2" ] && [ "$responsable2" == "res1" ]; then

    openssl enc -d -pbkdf2 -aes-256-cbc -in ./crypted/cleprincipale32.enc -out ./ramdisk/cleprincipale3.enc -pass file:<(cat ./ramdisk/cle2.clear)
    openssl enc -d -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale3.enc -out ./ramdisk/cleprincipale.clear -pass file:<(cat ./ramdisk/cle3.clear)
    rm ./ramdisk/cleprincipale3.enc
    rm ./ramdisk/cle2.clear
    rm ./ramdisk/cle3.clear



elif [ "$responsable1" == "res2" ] && [ "$responsable2" == "res2" ]; then

    openssl enc -d -pbkdf2 -aes-256-cbc -in ./crypted/cleprincipale34.enc -out ./ramdisk/cleprincipale3.enc -pass file:<(cat ./ramdisk/cle4.clear)
    openssl enc -d -pbkdf2 -aes-256-cbc -in ./ramdisk/cleprincipale3.enc -out ./ramdisk/cleprincipale.clear -pass file:<(cat ./ramdisk/cle3.clear)
    rm ./ramdisk/cleprincipale3.enc
    rm ./ramdisk/cle3.clear
    rm ./ramdisk/cle4.clear

else 
    echo "Error"
    exit 
fi




